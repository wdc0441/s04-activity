package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;

public interface PostService {
    // create a post
    void createPost(String token, Post post);

    // getting all posts
    Iterable<Post> getPosts();

}
